package org.fasttrackit.output;

import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankAccount;

public class AtmDispatcher {

    public static void withdraw(BankAccount account, int amount) {
        double balance = account.getBalance();
        if (balance < amount) {
            Display.showNotEnoughMoneyMsg();
            return;
        }
            account.updateBalance(balance - amount);




    }
}
