package org.fasttrackit.user;

public class CreditCustomer extends Person {
    public CreditCustomer(String firstName, String cnp, String lastName) {
        super(firstName, cnp, lastName);
    }

    @Override
    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }
}
