package org.fasttrackit.user;


import org.fasttrackit.input.UserInput;

public class BankCustomer extends Person {

    private final int id;


    private BankAccount bankAccount;

    private boolean isActive;


    public BankCustomer(int bId, String cnp, String firstName, String lastName) {
        super(firstName, cnp, lastName);
        this.id = bId;
        this.isActive = true;

    }

    public void deactivateAccount() {
        this.isActive = false;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }



    public boolean valiadtePin(String input) {
        return bankAccount.getCard().verifyPin(input);

    }
        public void updatePin(String changedPin) {
            bankAccount.getCard().updatePin(changedPin);


    }

    public String selectOptionMenu() {
        String option = UserInput.readFromKeyBoard();
        System.out.println("Person: " + getFirstName() + " selected: " + option);
        return option;
    }

    public boolean validateChangedPin(String changedPin, String confirmChangedPin) {
        return changedPin.equals(confirmChangedPin);
    }


    @Override
    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public void depositCash(int amount) {
        double balance = bankAccount.getBalance();
        bankAccount.updateBalance(balance + amount);
    }
}




