package org.fasttrackit.user;

public abstract class Person {


    private final String cnp;
    private final String firstName;
    private final String lastName;

    public Person(String firstName, String cnp, String lastName) {
        this.cnp = cnp;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    protected String getFirstName() {
        return firstName;
    }

    protected String getLastName() {
        return lastName;
    }

    public abstract String getFullName();
}
