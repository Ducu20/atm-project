package org.fasttrackit.ui;

import org.fasttrackit.employee.Employee;
import org.fasttrackit.input.UserInput;
import org.fasttrackit.user.BankAccount;
import org.fasttrackit.user.BankCustomer;
import org.fasttrackit.user.card.Card;

import static org.fasttrackit.AtmApp.ATM_Name;

public class Display {

    public static final String APP_MENU =
            """
                    Select an option:
                    1. Withdraw Cash
                    2. Check Balance
                    3. Change Pin
                    4. Activate Card
                    5. Deposit Cash
                    6. Deactivate Account
                    7. Show IBAN
                    8. Show card details
                            
                    """;


    public static void showWelcomeMsg() {
        System.out.println("Welcome to " + ATM_Name);
    }

    public static String askUserForPin() {
        System.out.println("Please enter your pin number:");
        return readFromKeyBoard();
    }

    public static String askForNewPin() {
        System.out.println("Please enter new pin number:");
        return readFromKeyBoard();
    }

    public static String confirmedNewPin() {
        System.out.println("Please confirm new pin number:");
        return readFromKeyBoard();
    }

    private static String readFromKeyBoard() {
        return UserInput.readFromKeyBoard();
    }

    public static void displayInvalidPinMsg() {
        System.out.println("Pin incorrect, please try again.");
    }

    public static void lockUserFor24h() {
        System.out.println("Incorrect Pin, Card locked for 24h.");
    }


    public static void showMenu() {
        System.out.println(APP_MENU);
    }

    public static int askUserForAmount() {
        System.out.println("Type in amount you want: ");
        return UserInput.readIntFromKeyBoard();
    }


    public static void showCustomerBalanceSheet(BankAccount bankAccount) {
        System.out.println("Your Balance is: " + bankAccount.getBalance() + " " + bankAccount.getCurrency());
    }

    public static void showNotEnoughMoneyMsg() {
        System.out.println("Your bank account does not have enough cash.");
    }

    public static void activateCardMessage(BankCustomer someOne) {
        System.out.printf("Thank you %s for activating the card. ", someOne.getFullName());
    }

    public static void informUserWithDeactivation() {
        System.out.println("Thank you for being a valuable customer. Hope you come back.");
    }

    public static void showIban(BankCustomer someOne) {
        System.out.println("IBAN: " + someOne.getBankAccount().getAccountNumber());

    }

    public static void showCardDetails(Card card) {
        System.out.println("Card details: " + card.getCardNumber());
    }

    public static void repairAtm(Employee employee) {
        System.out.println("Employee " + employee.getFullName() + " is repairing the ATM.");
    }
}



