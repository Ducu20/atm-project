package org.fasttrackit.Utils;

import org.fasttrackit.user.BankAccount;
import org.fasttrackit.user.BankCustomer;
import org.fasttrackit.user.card.Card;
import org.fasttrackit.user.card.CreditCard;

public class DataUtils {
    public static BankCustomer getCustomer() {

        BankCustomer customer = new BankCustomer(2, "18000010391", " Rami ", " Sharaiyri ");
        BankAccount account = new BankAccount("RO00RCNT01019185", "RON");
        account.updateBalance(150);
        Card card = new Card("5298759817365198", "3838");
        account.setCard(card);
        customer.setBankAccount(account);
        return customer;


    }
}
