package org.fasttrackit;

import org.fasttrackit.output.AtmDispatcher;
import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankCustomer;

public class UserActions {

    static void withdraw(BankCustomer someOne) {
        int amount = Display.askUserForAmount();
        AtmDispatcher.withdraw(someOne.getBankAccount(), amount);
        Display.showCustomerBalanceSheet(someOne.getBankAccount());
    }

    static void changePin(BankCustomer someOne) {
        String initialPin = Display.askUserForPin();
        boolean isValid = someOne.valiadtePin(initialPin);
        if (!isValid) {
            Display.displayInvalidPinMsg();
            return;


        }
        String changedPin = Display.askForNewPin();
        String confirmChangedPin = Display.confirmedNewPin();
        boolean pinMatches = someOne.validateChangedPin(changedPin, confirmChangedPin);
        if (!pinMatches) {
            Display.displayInvalidPinMsg();
            return;
        }
        someOne.updatePin(changedPin);
        System.out.println("Pin correct. Update pin: " + changedPin + " and try again.");
    }

     static void depositCash(BankCustomer someOne) {
        int amount = Display.askUserForAmount();
        someOne.depositCash(amount);
        Display.showCustomerBalanceSheet(someOne.getBankAccount());
    }

     static void deactivateAccount(BankCustomer someOne) {
        someOne.deactivateAccount();
        Display.informUserWithDeactivation();
    }
}
