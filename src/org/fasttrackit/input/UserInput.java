package org.fasttrackit.input;

import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankCustomer;

import java.io.InputStream;
import java.util.Scanner;

public class UserInput {

    public static String readFromKeyBoard() {
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);
        return keyboard.next();
    }

    public static int readIntFromKeyBoard() {
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);
        return keyboard.nextInt();
    }

    public static boolean authenticate(BankCustomer someOne) {
        for (int i = 0; i < 3; i++) {
            String pinCode = Display.askUserForPin();
            boolean isValid = someOne.valiadtePin(pinCode);
            if (isValid) {
                return true;
            }
            Display.displayInvalidPinMsg();
        }
        return false;
    }



}
