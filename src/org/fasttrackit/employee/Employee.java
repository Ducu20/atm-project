package org.fasttrackit.employee;

import org.fasttrackit.user.Person;

public class Employee extends Person {

    private final String title;

    public Employee(String firstName, String cnp, String lastName, String title) {
        super(firstName, cnp, lastName);
        this.title = title;
    }

    @Override
    public String getFullName() {
        return title + " " + getLastName() + ", " + getFirstName();
    }
}
