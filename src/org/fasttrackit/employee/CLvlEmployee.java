package org.fasttrackit.employee;

public class CLvlEmployee extends Employee {

    private final String signature;

    public CLvlEmployee(String firstName, String cnp, String lastName, String title, String signature) {
        super(firstName, cnp, lastName, title);
        this.signature = signature;
    }

    @Override
    public String getFullName() {
        return signature + "\n " + super.getFullName();
    }
}
