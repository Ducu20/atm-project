package org.fasttrackit;

import org.fasttrackit.employee.CLvlEmployee;
import org.fasttrackit.employee.Employee;
import org.fasttrackit.output.AtmDispatcher;
import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankCustomer;

import static org.fasttrackit.UserActions.*;
import static org.fasttrackit.Utils.DataUtils.getCustomer;
import static org.fasttrackit.input.UserInput.authenticate;

public class AtmApp {
    public static final String ATM_Name = "Free Cash ATM";

    public static void main(String[] args) {
        BankCustomer someOne = getCustomer();

        Display.showWelcomeMsg();

        boolean passThrough = authenticate(someOne);
        if (!passThrough) {
            Display.lockUserFor24h();
            return;
        }


        Display.showMenu();

        String option = someOne.selectOptionMenu();
        switch (option) {
            case "0" -> Display.repairAtm(initCLvlEmployee());
            case "1" -> withdraw(someOne);
            case "2" -> Display.showCustomerBalanceSheet(someOne.getBankAccount());
            case "3" -> changePin(someOne);
            case "4" -> Display.activateCardMessage(someOne);
            case "5" -> depositCash(someOne);
            case "6" -> deactivateAccount(someOne);
            case "7" -> Display.showIban(someOne);
            case "8" -> Display.showCardDetails(someOne.getBankAccount().getCard());

        }

    }

    private static Employee initEmployee() {
        return new Employee("Dorel", "1860601037891", "Dorica", "Mr.");
    }

    private static CLvlEmployee initCLvlEmployee() {
        return new CLvlEmployee("Tim", "1840904123456", "Cook", "Mr.", "CEO" );
    }

}


